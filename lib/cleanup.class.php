<?php
/**
* @package Knicklabs WP Cleanup
* @author  Nickolas Kenyeres
* @since  1.0
*
* This code was based on the Clean-Up module in the Roots/Soil plugin:
* @link https://github.com/roots/soil/blob/master/modules/clean-up.php
*/

if (!class_exists('Knicklabs_WP_Cleaner_Cleanup')) {
  class Knicklabs_WP_Cleaner_Cleanup
  {
    public function __construct()
    {
      // Cleanup the head
      add_action('init', array($this, 'cleanup'));

      // Remove the WordPress version from RSS feeds
      add_filter('the_generator', '__return_false');

      // Cleanup the language attributes
      add_filter('language_attributes', array($this, 'language_attributes'));

      // Cleanup output of stylesheet link tags
      add_filter('style_loader_tag', array($this, 'style_loader_tag'));

      // Add and remove body_class() classes
      add_filter('body_class', array($this, 'body_class'));

      // Wrap embedded media
      add_filter('embed_oembed_html', array($this, 'embed_oembed_html'));

      // Remove unnecessary dashboard widgets
      add_action('admin_init', array($this, 'remove_dashboard_widgets'));

      // Remove unnecessary admin widgets
      add_action('admin_init', array($this, 'remove_admin_widgets'));

      // Remove unnecessary menu items
      add_action('admin_menu', array($this, 'remove_admin_menu_items'));
      add_action('admin_init', array($this, 'remove_admin_menu_items_on_init'));

      // Remove update nag
      add_action('admin_menu', array($this, 'remove_update_nag'));

      // Remove update notifications
      add_filter('pre_site_transient_update_core', array($this, 'remove_update_notifications'));
      add_filter('pre_site_transient_update_plugins', array($this, 'remove_update_notifications'));
      add_filter('pre_site_transient_update_themes', array($this, 'remove_update_notifications'));

      // Remove unnecessary self-closing tags
      add_filter('get_avatar', array($this, 'remove_self_closing_tags'));
      add_filter('comment_id_fields', array($this, 'remove_self_closing_tags'));
      add_filter('post_thumbnail_html', array($this, 'remove_self_closing_tags'));

      // Don't return the default description in the RSS feed if it hasn't been changed
      add_filter('get_bloginfo_rss', array($this, 'remove_default_description'));

      // Fix for empty search queries redirecting to home page
      add_filter('request', array($this, 'request_filter'));

      // Replace 'howdy' message.
      add_filter('admin_bar_menu', array($this, 'replace_howdy'), 100);
    }

    /**
    * Clean up wp_head()
    *
    * Remove unnecessary <link> elements
    * Remove inline CSS from comments widget
    * Remove inline CSS from posts with galleries
    * Remove self-closing tags
    * Change single-quote to double-quote on rel_canonical()
    *
    * @since 1.0.0
    * @return void
    */
    public function cleanup()
    {
      // Originally from http://wpengineer.com/1438/wordpress-header/
      remove_action('wp_head', 'feed_links', 2);
      remove_action('wp_head', 'feed_links_extra', 3);
      remove_action('wp_head', 'rsd_link');
      remove_action('wp_head', 'wlwmanifest_link');
      remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
      remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0);

      global $wp_widget_factory;

      if (isset($wp_widget_factory->widgets['WP_Widget_Recent_Comments'])) {
        remove_action('wp_head', array($wp_widget_factory->widgets['WP_Widget_Recent_Comments'], 'recent_comments_style'));
      }

      if (!class_exists('WPSEO_Frontend')) {
        remove_action('wp_head', 'rel_canonical');
        add_action('wp_head', array($this, 'rel_canonical'));
      }
    }

    /**
    * Change single-quote to double-quote on rel_canonical()
    *
    * @since 1.0.0
    * @return void
    */
    public function rel_canonical()
    {
      global $wp_the_query;

      if (!is_singular()) {
        return;
      }

      if (!$id = $wp_the_query->get_queried_object_id()) {
        return;
      }

      $link = get_permalink($id);
      echo "\t<link rel=\"canonical\" href=\"$link\">\n";
    }

    /**
    * Cleanup the language attributes used in the HTML tag.
    *
    * @since 1.0.0
    * @return string
    */
    public function language_attributes()
    {
      $attributes = array();

      if (is_rtl()) {
        $attributes[] = 'dir="rtl"';
      }

      $lang = get_bloginfo('language');

      if ($lang) {
        $attributes[] = "lang=\"$lang\"";
      }

      $output = implode(' ', $attributes);
      $output = apply_filters('knicklabs/language_attributes', $output);

      return $output;
    }

    /**
    * Cleanup output of stylesheet link tags.
    *
    * @since 1.0.0
    * @return string
    */
    public function style_loader_tag($input)
    {
      preg_match_all("!<link rel='stylesheet'\s?(id='[^']+')?\s+href='(.*)' type='text/css' media='(.*)' />!", $input, $matches);

      // Only display media if it is meaningful
      $media = $matches[3][0] !== '' && $matches[3][0] !== 'all' ? ' media="' . $matches[3][0] . '"' : '';
      return '<link rel="stylesheet" href="' . $matches[2][0] . '"' . $media . '>' . "\n";
    }

    /**
    * Add and remove body_class() classes.
    *
    * @since 1.0.0
    * @return array
    */
    public function body_class($classes)
    {
      // Add post/page slug
      if (is_single() || is_page() && !is_front_page()) {
        $classes[] = basename(get_permalink());
      }

      // Remove unnecessary classes
      $home_id_class = 'page-id-' . get_option('page_on_front');
      $remove_classes = array(
      'page-template-default',
      $home_id_class
      );

      $classes = array_diff($classes, $remove_classes);
      return $classes;
    }

    /**
    * Wrap embedded media as suggested by Readability
    *
    * @link https://gist.github.com/965956
    * @link http://www.readability.com/publishers/guidelines#publisher
    *
    * @since 1.0.0
    * @return string
    */
    public function embed_oembed_html($cache)
    {
      return '<div class="entry-content-asset">' . $cache . '</div>';
    }

    /**
    * Remove unnecessary dashboard widgets.
    *
    * @link http://www.deluxeblogtips.com/2011/01/remove-dashboard-widgets-in-wordpress.html
    *
    * @since 1.0.0
    * @return void
    */
    public function remove_dashboard_widgets()
    {
      remove_action('welcome_panel', 'wp_welcome_panel');
      remove_meta_box('dashboard_activity', 'dashboard', 'normal');
      remove_meta_box('dashboard_right_now', 'dashboard', 'normal');
      remove_meta_box('dashboard_recent_comments', 'dashboard', 'normal');
      remove_meta_box('dashboard_incoming_links', 'dashboard', 'normal');
      remove_meta_box('dashboard_plugins', 'dashboard', 'plugins');
      remove_meta_box('dashboard_primary', 'dashboard', 'normal');
      remove_meta_box('dashboard_quick_press', 'dashboard', 'normal');
      remove_meta_box('dashboard_recent_drafts', 'dashboard', 'normal');
      remove_meta_box('dashboard_secondary', 'dashboard', 'normal');
    }

    /**
    * Remove unnecessary admin widgets.
    *
    * @link http://codex.wordpress.org/Function_Reference/remove_meta_box
    *
    * @since 1.0.0
    * @return void
    */
    public function remove_admin_widgets() {
      remove_meta_box('trackbacksdiv', 'post', 'normal');
      remove_meta_box('commentsdiv', 'post', 'normal');
      remove_meta_box('formatdiv', 'post', 'normal');
      remove_meta_box('postcustom', 'post', 'normal');
      remove_meta_box('slugdiv', 'post', 'normal');
      remove_meta_box('authordiv', 'post', 'normal');

      remove_meta_box('commentsdiv', 'page', 'normal');
      remove_meta_box('postcustom', 'page', 'normal');
      remove_meta_box('slugdiv', 'page', 'normal');
      remove_meta_box('authordiv', 'page', 'normal');
    }

    /**
     * Remove admin menu items
     * @since 1.0.0
     * @return void
     */
    public function remove_admin_menu_items_on_init() {
      remove_submenu_page('themes.php', 'theme-editor.php');
    }

    /**
    * Remove admin menu items
    *
    * @link http://codex.wordpress.org/Function_Reference/remove_menu_page
    *
    * @since 1.0.0
    * @return void
    */
    public function remove_admin_menu_items() {
      remove_submenu_page('index.php', 'index.php');
      remove_submenu_page('index.php', 'update-core.php');
      remove_submenu_page('plugins.php', 'plugin-editor.php' );
    }

    /**
    * Remove update nag
    *
    * @link http://www.wpoptimus.com/626/7-ways-disable-update-wordpress-notifications/
    *
    * @since 1.0
    * @return void
    */
    public function remove_update_nag() {
      remove_action('admin_notices', 'update_nag', 3);
    }

    /**
    * Remove update notifications
    *
    * @link http://www.wpoptimus.com/626/7-ways-disable-update-wordpress-notifications/
    *
    * @since 1.0
    * @return void
    */
    public function remove_update_notifications() {
      global $wp_version;
      return(object) array('last_checked'=> time(),'version_checked'=> $wp_version);
    }

    /**
    * Remove unnecessary self-closing tags.
    *
    * @since 1.0.0
    * @return string
    */
    public function remove_self_closing_tags($input)
    {
      return str_replace(' />', '>', $input);
    }

    /**
    * Don't return the default description in the RSS feed if it hasn't been changed.
    *
    * @since 1.0.0
    * @return string
    */
    function remove_default_description($bloginfo)
    {
      $default_tagline = 'Just another WordPress site';
      return ($bloginfo === $default_tagline) ? '' : $bloginfo;
    }

    /**
    * Fix for empty search queries redirecting to homepage.
    *
    * @link http://wordpress.org/support/topic/blank-search-sends-you-to-the-homepage#post-1772565
    * @link http://core.trac.wordpress.org/ticket/11330
    *
    * @since 1.0.0
    * @return array
    */
    function request_filter($query_vars)
    {
      if (isset($_GET['s']) && empty($_GET['s']) && !is_admin()) {
        $query_vars['s'] = ' ';
      }

      return $query_vars;
    }

    /**
     * Replace the 'howdy' message.
     *
     * @since 1.0.0
     * @return void
     */
    function replace_howdy($wp_admin_bar) {
      $my_account = $wp_admin_bar->get_node('my-account');
      $title = str_replace('Howdy', 'Hello', $my_account->title);
      $wp_admin_bar->add_node(array('id' => 'my-account', 'title' => $title));
    }
  }

  new Knicklabs_WP_Cleaner_Cleanup();
}
