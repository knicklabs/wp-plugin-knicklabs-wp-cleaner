<?php
/**
* @package Knicklabs WP Cleaner
* @author  Nickolas Kenyeres
* @since  1.0
*
* This code was based on the Nice Search module in the Roots/Soil plugin:
* @link https://github.com/roots/soil/blob/master/modules/nice-search.php
*/

if (!class_exists('Knicklabs_WP_Cleaner_Search')) {
  class Knicklabs_WP_Cleaner_Search {
    public function __construct()
    {
      // Redirect search results from ?s=query to search/query
      add_action('template_redirect', array($this, 'template_redirect'));
    }

    /**
    * Redirects search results from /?s=query to /search/query/, converts %20 to +
    *
    * @link http://txfx.net/wordpress-plugins/nice-search/
    *
    * @since 1.0.0
    * @return void
    */
    public function template_redirect()
    {
      global $wp_rewrite;

      if (!isset($wp_rewrite) || !is_object($wp_rewrite) || !$wp_rewrite->using_permalinks()) {
        return;
      }

      $search_base = $wp_rewrite->search_base;

      if (is_search() && !is_admin() && strpos($_SERVER['REQUEST_URI'], "/{$search_base}/") === false) {
        wp_redirect(home_url("/{$search_base}/" . urlencode(get_query_var('s'))));
        exit();
      }
    }
  }

  new Knicklabs_WP_Cleaner_Search();
}
