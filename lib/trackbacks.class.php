<?php
/**
* @package Knicklabs WP Cleaner
* @author  Nickolas Kenyeres
* @since  1.0
*
* This code was based on the Disable Trackbacks module in the Roots/Soil plugin:
* @link https://github.com/roots/soil/blob/master/modules/disable-trackbacks.php
*/

if (!class_exists('Knicklabs_WP_Cleaner_Trackbacks')) {
  class Knicklabs_WP_Cleaner_Trackbacks {
    public function __construct()
    {
      // Disable pingback XMLRPC method
      add_filter('xmlrpc_methods', array($this, 'xmlrpc_methods'), 10, 1);

      // Remove pingback header
      add_filter('wp_headers', array($this, 'wp_headers'), 10, 1);

      // Kill trackback rewrite rule
      add_filter('rewrite_rules_array', array($this, 'rewrite_rules_array'));

      // Kill bloginfo('pingback_url')
      add_filter('bloginfo_url', array($this, 'bloginfo_url'), 10, 2);

      // Disable XMLRPC call
      add_action('xmlrpc_call', array($this, 'xmlrpc_call'));
    }

    /**
    * Disable pingback XMLRPC method
    *
    * @since 1.0.0
    * @param $methods array
    * @return array
    */
    public function xmlrpc_methods($methods)
    {
      unset($methods['pingback.ping']);
      return $methods;
    }

    /**
    * Remove pingback header
    *
    * @since 1.0.0
    * @param $headers array
    * @return array
    */
    public function wp_headers($headers)
    {
      if (isset($headers['X-Pingback'])) {
        unset($headers['X-Pingback']);
      }

      return $headers;
    }

    /**
    * Kill trackback rewrite rule
    *
    * @since 1.0.0
    * @param $rules array
    * @return array
    */
    public function rewrite_rules_array($rules)
    {
      foreach ($rules as $rule => $rewrite) {
        if (preg_match('/trackback\/\?\$$/i', $rule)) {
          unset($rules[$rule]);
        }
      }
      return $rules;
    }

    /**
    * Kill bloginfo('pingback_url')
    *
    * @since 1.0.0
    * @param $output string
    * @param $show string
    * @return string
    */
    public function bloginfo_url($output, $show)
    {
      if ($show == 'pingback_url') {
        $output = '';
      }

      return $output;
    }

    /**
    * Disable XMLRPC call
    *
    * @since 1.0.0
    * @param $action string
    * @return void
    */
    public function xmlrpc_call($action)
    {
      if ('pingback.png' === $action) {
        wp_die(
        'Pingbacks are not supported',
        'Not Allowed!',
        array('response' => 403)
        );
      }
    }
  }

  new Knicklabs_WP_Cleaner_Trackbacks();
}
