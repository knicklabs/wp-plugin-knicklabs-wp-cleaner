<?php
/**
* @package Knicklabs WP Cleander
* @author  Nickolas Kenyeres
* @since  1.0
*
* @link http://codex.wordpress.org/Dashboard_Widgets_API
*/

if (!class_exists('Knicklabs_WP_Cleaner_Dashboard')) {
  class Knicklabs_WP_Cleaner_Dashboard {
    public function __construct() {
      add_action('wp_dashboard_setup', array($this, 'add_service_and_support_dashboard_widget'));
    }

    /**
    * Add a service and support widget to the dashboard
    */
    function add_service_and_support_dashboard_widget() {
      wp_add_dashboard_widget(
      'knicklabs_support_dashboard_widget',
      __('Service &amp; Support', 'knicklabs'),
      array($this, 'knicklabs_support_dashboard_widget_callback')
    );
  }

  /**
  * Output the contents of the the service and support widget
  */
  function knicklabs_support_dashboard_widget_callback() {
      ?>
        <p><?php _e('Need assistance? Contact the developer (Nickolas Kenyeres/@knicklabs) by email.', 'knicklabs'); ?></p>
        <p><a href="mailto:nkenyeres@gmail.com" class="button button-primary"><?php _e('Email Now', 'knicklabs'); ?></a></p>
      <?php
    }
  }

  new Knicklabs_WP_Cleaner_Dashboard();
}
