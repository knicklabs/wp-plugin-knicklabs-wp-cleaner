<?php 
/**
 * Plugin Name: Knicklabs WP Cleaner
 * Plugin URL:  http://github.com/knicklabs
 * Description: This plugin cleans up the WordPress admin panel and theme output.
 * Version:     1.0.0
 * Author:      Nickolas Kenyeres
 * Author URI:  http://github.com/knicklabs
 */
if (!defined('ABSPATH')) exit;

require_once('lib/cleanup.class.php');
require_once('lib/dashboard.class.php');
require_once('lib/search.class.php');
require_once('lib/trackbacks.class.php');